variable "aws_region" {
  type    = string
  default = "eu-central-1"
}

#CSP Accounts
variable "aws_access_key" { type = string }
variable "aws_secret_key" { type = string }

variable "ssh_key" { type = string }
variable "psk" {
  type = string
  default = "dhjo8cuoo2832XD3ZASDZCesaSADASD54574"
}