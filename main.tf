resource "aws_key_pair" "user" {
  key_name   = "user_key"
  public_key = var.ssh_key
}

module "branch1" {
  source = "./modules/branch"

  branch_id          = 1
  region             = var.aws_region
  fortigate_password = "Aviatrix#1234"
  iam_role_name      = aws_iam_instance_profile.instance_role.name
  ssh_key            = var.ssh_key
  vpn_pre_shared_key = var.psk
  remote_subnet      = "192.168.2.0 255.255.255.0"
}

module "branch2" {
  source = "./modules/branch"

  branch_id          = 2
  region             = var.aws_region
  fortigate_password = "Aviatrix#1234"
  iam_role_name      = aws_iam_instance_profile.instance_role.name
  ssh_key            = var.ssh_key
  vpn_pre_shared_key = var.psk
  remote_subnet      = "192.168.1.0 255.255.255.0"
}

#Create IAM role and policy for the FW instance to access the bucket.
resource "aws_iam_role" "bootstrap" {
  name               = "bootstrap"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "bootstrap" {
  name   = "bootstrap"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:Get*",
                "s3:List*"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "policy_role" {
  role       = aws_iam_role.bootstrap.name
  policy_arn = aws_iam_policy.bootstrap.arn
}

resource "aws_iam_instance_profile" "instance_role" {
  name = "bootstrap"
  role = aws_iam_role.bootstrap.name
}