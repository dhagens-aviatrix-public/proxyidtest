config system global
    set hostname "branch_${branch_id}"
    set timezone 04
    set admintimeout 60
end

config system admin
    edit "admin"
        set accprofile "super_admin"
        set vdom "root"
        set password "${password}"
        set ssh-public-key1 "${ssh_key}"
    next
end

config vpn ipsec phase1-interface
    edit "TO_REMOTE"
        set interface "port1"
        set ike-version 2
        set keylife 28800
        set proposal aes256-sha256
        set remote-gw ${remote_ip}
        set psksecret ${pre_shared_key}
        set dpd-retryinterval 5
        set dpd on-demand        
    next
end

config vpn ipsec phase2-interface
    edit "TO_REMOTE"
        set phase1name "TO_REMOTE"
        set proposal aes256-sha256
        set keepalive enable
        set keylifeseconds 1800
        set auto-negotiate enable
        set src-addr-type subnet
        set dst-addr-type subnet
        set src-subnet 192.168.${branch_id}.0 255.255.255.0
        set dst-subnet ${remote_subnet}
    next
end

config firewall policy
    edit 1
        set name "Any-IN"
        set srcintf "TO_REMOTE"
        set dstintf "port1"
        set srcaddr "all"
        set dstaddr "all"
        set action accept
        set schedule "always"
        set service "ALL"
        set logtraffic all
    next
    edit 2
        set name "Any-OUT"
        set srcintf "port1"
        set dstintf "TO_REMOTE"
        set srcaddr "all"
        set dstaddr "all"
        set action accept
        set schedule "always"
        set service "ALL"
        set logtraffic all
    next
end

config router static
    edit 10
        set dst ${remote_subnet}
        set device "TO_REMOTE"
    next
    edit 200
        set dst 10.0.0.0 255.0.0.0
        set blackhole enable
        unset distance
    next
    edit 201
        set dst 172.16.0.0 255.240.0.0
        set blackhole enable
        unset distance
    next
end