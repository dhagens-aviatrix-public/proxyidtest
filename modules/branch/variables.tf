variable "branch_id" {
    type = string
}

variable "remote_subnet" {
    type = string
}

variable "remote_ip" {
    type = string
    default = "1.1.1.1"
}

variable "region" {
    type = string
    default = "eu-central-1"
}

variable "ssh_key" {
    type = string
}

variable "fortigate_password" {
    type = string
}

variable "iam_role_name" {
    type = string
}

variable "vpn_pre_shared_key" {
  type    = string
}

variable "fortios_version" {
  type    = string
  default = "6.2.5"
}

#DNS Suffix for creating records
variable "pub_dns_suffix" {
  type    = string
  default = "aws.avxlab.nl"
}
variable "priv_dns_suffix" {
  type    = string
  default = "aws.avxlab.int"
}


